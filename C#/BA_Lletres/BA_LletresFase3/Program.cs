﻿using System;
using System.Collections.Generic;

namespace BA_LletresFase3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bon dia! Introduce tu nombre, que vamos a jugar un rato...");
            string name = Console.ReadLine();
            List<char> listCharactersNom = new List<char>();
            Dictionary<string, int> counterLletres = new Dictionary<string, int>();
            

            for (int i = 0; i < name.Length; i++)
            {
                if (Char.IsDigit(name[i]) || Char.IsWhiteSpace(name[i]))
                {
                    Console.WriteLine("Els noms de persones no contenen números ni espacios! mequetrefe!");
                }
                else
                {
                    char lletreUp = Char.ToUpper(name[i]);
                    listCharactersNom.Add(lletreUp);
                    string lletreStr = lletreUp.ToString().Normalize();

                    if (lletreStr == "A" || lletreStr == "E" || lletreStr == "I" || lletreStr == "O" || lletreStr == "U")
                        Console.WriteLine("This is a vowel!!");
                    else
                        Console.WriteLine("This is a consonant!!");

                    bool isInDictionary = false;
                    foreach (var item in counterLletres.Keys)
                    {
                        if (item == lletreStr)
                            isInDictionary = true;
                    }
                    
                    if (isInDictionary == false)
                    {
                        int counter = 0;
                        foreach (char lle in name)
                        {
                            char lleUp = Char.ToUpper(lle);
                            string lleStr = lleUp.ToString().Normalize();
                            if (lletreStr == lleStr)
                                counter += 1;
                        }
                        counterLletres.Add(lletreStr, counter);
                    }
                }
            }
            Console.Write("\r\nPulsa un tecla para saber el número de veces que se repiten las letras en tu nombre!");
            Console.ReadLine();

            foreach (KeyValuePair<string, int> data in counterLletres)
            {
                Console.WriteLine("El número de veces que la letra {0} está en tu nombre es {1}", data.Key, data.Value);
            }
        }
    }
}
