﻿using System;

namespace BA_LletresFase1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bon dia! Introduce tu nombre, que vamos a jugar un rato...");
            string name = Console.ReadLine();
            char[] arrayCharactersNom = new char[name.Length];

            for (int i = 0; i < name.Length; i++)
            {
                if (Char.IsDigit(name[i]) || Char.IsWhiteSpace(name[i]))
                {
                    Console.WriteLine("Els noms de persones no contenen números ni espacios! mequetrefe!");
                }
                else
                {
                    char lletreUp = Char.ToUpper(name[i]);
                    arrayCharactersNom[i] = lletreUp;
                    Console.WriteLine(lletreUp.ToString().Normalize());
                }
            }
            
        }
    }
}
